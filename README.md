# Swagger UI

Non root user Docker [Swagger UI express](https://github.com/scottie1984/swagger-ui-express) for Openshift.

## Usage

### CLI

```
docker run -it -p 8000:3000 -v /local/path/to/options.json:/options.json vmaillart/swagger-ui
```

And open http://localhost:8000

### Docker compose

```
version: "3"

services:
  swagger-ui:
    image: vmaillart/swagger-ui
    volumes:
      - /local/path/to/options.json:/options.json
    ports:
      - "8000:3000"
```

### Options

Example of options.json file :

```
{
  "swaggerUrl": "https://petstore.swagger.io/v2/swagger.json",
  "swaggerUrls": [
    { "url": "https://petstore.swagger.io/v2/swagger.json", "name": "name1" },
    { "url": "https://petstore.swagger.io/v2/swagger.json", "name": "name2" },
    { "url": "https://petstore.swagger.io/v2/swagger.json", "name": "name3" }
  ]
}
```